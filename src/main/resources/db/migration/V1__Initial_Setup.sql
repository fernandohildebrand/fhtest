
--CREATE SCHEMA IF NOT EXISTS fhtest;

CREATE TABLE product(id BIGINT PRIMARY KEY, name VARCHAR(255), product_parent BIGINT, FOREIGN KEY(product_parent) REFERENCES product(id));

CREATE TABLE image(id BIGINT PRIMARY KEY, name VARCHAR(255), product_id BIGINT, FOREIGN KEY(product_id) REFERENCES product(id));


INSERT INTO product (id, name) VALUES (1, 'Product Name 1');

INSERT INTO product (id, name, product_parent) VALUES (2, 'Product Name 2', 1);

INSERT INTO image (id, name, product_id) VALUES (1, 'Image Name 1',1);
