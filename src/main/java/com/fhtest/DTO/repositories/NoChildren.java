package com.fhtest.DTO.repositories;

import com.fhtest.DTO.entities.Product;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

/**
 * Created by fernando on 15/09/17.
 */

public interface NoChildren {

    String getId();

    String getName();

}
