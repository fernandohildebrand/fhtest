package com.fhtest.DTO.repositories;

import com.fhtest.DTO.entities.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;


/**
 * All transactions in this class are marked as read only.  If a transaction needs to modify the datasource
 * mark the method modifying the datasource with @Modifying and @Transactional annotation.
 *
 * @see <a href=https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#transactional-query-methods </a>
 */

//@RepositoryRestResource(excerptProjection = Product.class)
public interface ProductRepository extends CrudRepository<Product, Long> {

    Collection<NoChildren> findAllNoChildren();

}