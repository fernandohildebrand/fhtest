package com.fhtest.DTO.repositories;

import com.fhtest.DTO.entities.Product;
import org.springframework.data.rest.core.config.Projection;

import java.util.Set;

/**
 * Created by fernando on 15/09/17.
 */
@Projection(name = "allChildren", types = Product.class)
public interface AllChildren {

    Long getId();
    String getName();
    Set<Product> getProductChildId();

}
