package com.fhtest.DTO.repositories;

import com.fhtest.DTO.entities.Image;
import com.fhtest.DTO.entities.Product;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by fernando on 15/09/17.
 */
public interface ImageRepository extends CrudRepository<Image, Long> {
}
