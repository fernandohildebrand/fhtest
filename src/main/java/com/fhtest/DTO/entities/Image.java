package com.fhtest.DTO.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;

import java.io.Serializable;

/**
 * Created by fernando on 12/09/17.
 */

@Entity
@Table(name = "image")
public class Image implements Serializable{

    /**
     * If we do not specify a generator value in the @GeneratedValue annotation hibernate will attempt to use by default
     * "hibernate_sequence".  This does not exist in the schema and needs to be created.  However if you want to create
     * and specify your own sequence you can do so in Postgres and specify the sequence name in the @GeneratedValue
     * annotation.
     */

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "product_id")
    @JsonManagedReference
    private Product product;


    public Image() {
    }

    public Image(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Product getProduct() {
        return this.product;
    }

    public void setProduct(Product product){
        this.product = product;
    }


}
