# README #

### What is this repository for? ###

* Quick summary - This repository serves as an example of technological choices that were made to solve the following problems: DB Migration, Fast Prototyping of a persistence layer with RESTful boundaries.
* Version - 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up - This project uses spring boot which drops a war to be consumed by some container.  In running this example there is a container to execute the war.
* * I used a local install of Tomcat 8 and created a new runtime config in intellij.  Refer to your distribution instructions on setting up Tomcat locally.
* * * postgres:9.4.4 is the version of postgres image to pull for container construction
* * Note:  Spring Data Rest uses the HAL format for RESTFull endpoints mapping: http://stateless.co/hal_specification.html
* How to run tests - Make calls to the service with tool of choice.  The RESTClient plugin in Firefox to test PUT, GET, POST and DELETE.
* Deployment instructions - Build war, drop in container and use the above environment variables.  I used IntelliJ to create a new Tomcat run configuration and ran within IntelliJ.
* The application can be run without IDE using: mvn spring-boot:run
* If desired, can be run in debug mode using: mvn spring-boot:run -X
* To access the H2 database (while application is running), just point your browser to: http://localhost/console with the jdbc url jdbc:h2:mem:fhtest

*RESTFull Crud instructions:
**You can issue POST, PUT, PATCH, and DELETE REST calls to either replace, update, or delete existing records using curl.
**Examples:
**          curl -i -X POST -H "Content-Type:application/json" -d { \"name\": \"Product1\", \"product_parent\": \"1\" }" http://localhost:8080/product
**          curl -i -X POST -H "Content-Type:application/json" -d "{ \"name\": \"Image1\", \"product_id\": \"1\" }" http://localhost:8080/image
**
**          curl -X PUT -H "Content-Type:application/json" -d "{ \"name\": \"Product1\", \"product_parent\": \"1\" }" http://localhost:8080/product/1
**          curl -X PUT -H "Content-Type:application/json" -d "{ \"name\": \"Image1\", \"product_id\": \"1\" }" http://localhost:8080/image/1
**
**          curl -X PATCH -H "Content-Type:application/json" -d "{ \"name\": \"Product1.1\" }" http://localhost:8080/product/1
**          curl -X PATCH -H "Content-Type:application/json" -d "{ \"name\": \"Image1.1\" }" http://localhost:8080/image/1
**
**          curl -X DELETE http://localhost:8080/product/1
**          curl -X DELETE http://localhost:8080/image/1
**
**OBS: To find all custom queries use: curl http://localhost:8080/product/search

*To Get all products excluding relationships use: http://localhost:8080/products?projection=noChildren
*To Get all products including specified relationships : http://localhost:8080/products
*To Get a product excluding relationships use: http://localhost:8080/
*To Get a product including specified relationships : http://localhost:8080/products/search/findById?id={id}
*To Get set of child products for specific product
*To Get set of images for specific product


### Who do I talk to? ###

* Repo owner or admin - fernandohildebrand@gmail.com